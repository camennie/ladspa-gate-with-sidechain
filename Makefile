# Makefile

# Compiler name
CC=gcc

# Libraries to be included
#LDLIBS=`pkg-config jack alsa gtkmm-2.4 pangomm-1.4 libglademm-2.4 gdkmm-2.4 libxml++-2.6 --libs` -lboost_filesystem -lboost_serialization -lboost_thread
#LDLIBS=-g -O2 -Wall -O6 -fomit-frame-pointer -fstrength-reduce -funroll-loops -ffast-math -fPIC -DPIC -module -avoid-version -Wc,-nostartfiles  -o gsm_1215.la -rpath /usr/local/lib/ladspa  gsm_1215.lo gsm/libgsm.a -lrt -lm -lm  -lm
LDLIBS=-module -avoid-version -Wc,-nostartfiles -lrt -lm -nostartfiles

# Flagas
#CXXFLAGS=-Wall -g `pkg-config jack alsa gtkmm-2.4 pangomm-1.4 libglademm-2.4 gdkmm-2.4 libxml++-2.6 --cflags` -I.
CFLAGS=-O2 -Wall -O6 -fomit-frame-pointer -fstrength-reduce -funroll-loops -ffast-math -fPIC -DPIC

# Variables
OBJS = gate_5410_SC.o

#Application name
gate_5410_SC.so: $(OBJS)
	$(CC) -shared -Wl,-soname,gate_5410_SC.so $(OBJS) $(LDLIBS)  -o gate_5410_SC.so

%.o : %.c
	$(COMPILE.c) -MD -o $@ $<
	@cp $*.d $*.P; \
        sed -e 's/#.*//' -e 's/^[^:]*: *//' -e 's/ *\\$$//' \
            -e '/^$$/ d' -e 's/$$/ :/' < $*.d >> $*.P; \
        rm -f $*.d


clean:
	rm -f *.o *.so

PHONY: clean

